import React, { Component } from "react";
import styles from "./style.css";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { registration } from "../../../framework/modules/auth/actions";

class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
  }

  onSubmit = event => {
    event.preventDefault();
    this.props.registration(this.state.email, this.state.password).then(() => {
      this.props.history.push("/auth");
    });
  };

  handleItemChange = event => {
    this.setState({
      ...this.state,
      [event.target.id]: event.target.value
    });
  };

  render() {
    return (
      <form name="authForm" className="authForm">
        <span className="auth_form_center">
          <div className="content_container">
            <div className="input_container">
              <p>Адрес</p>
              <input
                type="text"
                size="40"
                value={this.state.email}
                onChange={this.handleItemChange}
                id="email"
              />
            </div>
            <div className="input_container">
              <p>Пароль</p>
              <input
                type="password"
                size="40"
                value={this.state.password}
                onChange={this.handleItemChange}
                id="password"
              />
            </div>
            <div className="input_container">
              <input
                type="button"
                size="40"
                value="Зарегистрироваться"
                onClick={this.onSubmit}
              />
            </div>
          </div>
        </span>
        <Link to={`/auth`}>Авторизироваться</Link>
      </form>
    );
  }
}

export default connect(
  state => ({ state: state.auth }),
  { registration }
)(Registration);
