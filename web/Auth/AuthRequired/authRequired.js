import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { TOKEN } from "../../../framework/services/storeConsts";

export default function requiredAuth(Comp) {
  class AuthRequired extends Component {
    constructor(props) {
      super(props);

      const response = localStorage.getItem(TOKEN);
      const token = response ? JSON.parse(response) : null;
      this.state = {
        token: token.id,
        tokenCratedAt: token.crated,
        tokenLiveTime: token.ttl
      };
    }

    render() {
      if (
        this.state.token === null ||
        this.state.token === undefined ||
        this.state.token == ""
      ) {
        console.log(this.state);
        return <Redirect to="/auth" />;
      } else {
        return <Comp history={this.props.history} />;
      }
    }
  }

  return connect(state => ({ state: state.auth }))(AuthRequired);
}
