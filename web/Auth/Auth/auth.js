import React, { Component } from "react";
import styles from "./style.css";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { auth } from "../../../framework/modules/auth/actions";

class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: props.state.email,
      password: ""
    };
  }
  onSubmit = event => {
    event.preventDefault();

    this.props.auth(this.state.email, this.state.password).then(() => {
      this.props.history.push("/");
    });
  };

  handleItemChange = event => {
    this.setState({
      ...this.state,
      [event.target.id]: event.target.value
    });
  };

  render() {
    return (
      <div className="auth_form">
        <span className="auth_form_center">
          <div className="content_container">
            <div className="input_container">
              <p>Адрес</p>
              <input
                type="text"
                size="40"
                value={this.state.email}
                onChange={this.handleItemChange}
                id="email"
              />
            </div>
            <div className="input_container">
              <p>Пароль</p>
              <input
                type="password"
                size="40"
                value={this.state.password}
                onChange={this.handleItemChange}
                id="password"
              />
            </div>
            <div className="input_container">
              <input
                type="button"
                size="40"
                value="Войти"
                onClick={this.onSubmit}
              />
            </div>
          </div>
          <Link to={`/registration`}>Зарегистрироваться</Link>
        </span>
      </div>
    );
  }
}

export default connect(
  state => ({ state: state.auth }),
  { auth }
)(Auth);
