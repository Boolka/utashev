import React from 'react'
import ReactDOM from 'react-dom'
import './index.scss'
import { HashRouter } from 'react-router-dom'
import App from './App'
import registerServiceWorker from './registerServiceWorker'
import { Provider } from 'react-redux'
import store from '../framework/store'
import Storage from '../framework/services/tokenService'
import Network from '../framework/services/networkService'
import 'idempotent-babel-polyfill';

const tokenService = new Storage(localStorage);
const networkService = new Network(tokenService);
const services = { tokenService, networkService }

ReactDOM.render(
  <Provider store={store(services)}>
    <HashRouter>
      <App/>
    </HashRouter>
  </Provider>,
  document.getElementById('root')
)
registerServiceWorker()
module.hot.accept();