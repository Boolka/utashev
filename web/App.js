import React from "react";
import { Route, Switch } from "react-router-dom";

import Notes from "./Notes/notes";
import Registration from "./Auth/Registration/registration";
import Auth from "./Auth/Auth/auth";

class App extends React.Component {
  render() {
    return (
        <Switch>
          <Route exact path="/" component={Notes} />
          <Route path="/registration" component={Registration} />
          <Route path="/auth" component={Auth} />
        </Switch>
    );
  }
}

export default App;
