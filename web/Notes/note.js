import React, { Component } from "react";
import authRequired from "../Auth/AuthRequired/authRequired";
import { connect } from "react-redux";
import styles from "./style.scss";

class Note extends Component {
  renderCheckBox() {
    return (
      <span // Checker
        className={
          this.props.state.done
            ? "note-done note-checker"
            : "note-undone note-checker"
        }
        onClick={() =>
          this.props.onCompleat(
            this.props.state.id,
            this.props.state.done,
            this.props.state.title,
            this.props.state.body
          )
        }
      />
    );
  }
  render() {
    return (
      <span
        className="note-content"
        onClick={event => {
          if (event.target == event.currentTarget) {
            this.props.onOpenNote(this.props.index);
          }
        }}
      >
        <div className="note-name">{this.props.state.title}</div>
        <div className="note-body">{this.props.state.body}</div>
        {this.renderCheckBox()}
        <button // Delete
          className="note-delete"
          onClick={() => this.props.onDelete(this.props.state.id)}
        >
          Удалить
        </button>
      </span>
    );
  }
}

export default Note;
