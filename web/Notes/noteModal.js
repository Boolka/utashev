import React, { Component } from "react";
import authRequired from "../Auth/AuthRequired/authRequired";
import { connect } from "react-redux";
import styles from "./style.scss";

class NoteModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: this.props.state.id || -1,
      done: this.props.state.done || false,
      title: this.props.state.title || "",
      body: this.props.state.body || "",
      isEditing: this.props.isEditing || false
    };
  }

  closeModal() {
    if (this.state.isEditing) {
      if (!window.confirm("Закрыть форму без сохранения изменений?")) {
        return;
      }
    }
    this.props.onCloseNote();
  }

  handleNotecreatorChange = event => {
    this.setState({
      ...this.state,
      [event.target.id]: event.target.value
    });
  };

  componentWillReceiveProps(props) {
    this.setState(props.state);
  }

  renderTitle() {
    return this.state.isEditing ? ( // Title
      <div className="input_container">
        <input
          type="text"
          size="20"
          className="info-label"
          value={this.state.title}
          onChange={this.handleNotecreatorChange}
          id="title"
        />
      </div>
    ) : (
      <div className="input_container">
        <div className="note-name">{this.state.title}</div>
      </div>
    );
  }
  renderBody() {
    return (
      <div className="input_container">
        <textarea
          rows="10"
          cols="45"
          onChange={this.handleNotecreatorChange}
          value={this.state.body}
          disabled={this.state.isEditing ? "" : "disabled"}
          id="body"
        >
          {this.state.body}
        </textarea>
      </div>
    );
  }
  renderCheckbox() {
    return (
      <span // Checker
        className={
          this.state.done
            ? "note-done note-checker"
            : "note-undone note-checker"
        }
        onClick={() =>
          this.props.onCompleat(
            this.state.id,
            this.state.done,
            this.state.title,
            this.state.body
          )
        }
      />
    );
  }
  renderEditButton() {
    return (
      <button // Edit
        onClick={() => {
          this.setState({ isEditing: !this.state.isEditing });
          this.state.isEditing
            ? this.props.onEdit(
                this.state.title,
                this.state.body,
                this.state.done,
                this.state.id
              )
            : null;
        }}
      >
        {this.state.isEditing ? "Сохранить" : "Редактировать"}
      </button>
    );
  }

  render() {
    return (
      <span className="note-modal">
        {this.renderTitle()}
        {this.renderBody()}
        {this.renderCheckbox()}
        {this.renderEditButton()}
        <button onClick={() => this.closeModal()}> Закрыть </button>
      </span>
    );
  }
}

export default NoteModal;
