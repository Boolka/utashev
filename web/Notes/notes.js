import React, { Component } from "react";
import authRequired from "../Auth/AuthRequired/authRequired";
import { connect } from "react-redux";
import styles from "./style.scss";
import Note from "./note";
import NoteModal from "./noteModal";
import {
  getNotes,
  createNote,
  noteUpdate,
  deleteNote,
  undo,
  redo
} from "../../framework/modules/notes/actions";
import { bindActionCreators } from "redux";

function mapStateToProps(state, ownProps) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    { getNotes, createNote, noteUpdate, deleteNote, undo, redo },
    dispatch
  );
}

class Notes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: {
        isOpened: false,
        index: 0,
        isEditing: false
      },
    };
    this.props
      .getNotes()
      .then(response => this.setState({ notes: response }));
  }

  onCompleat = (id, done, title, body) => {
    this.props.noteUpdate(title, body, !done, id);
  };

  onDelete = id => {
    this.props.deleteNote(id);
  };

  onEdit = (title, body, done, id) => {
    this.props.noteUpdate(title, body, done, id);
  };

  openNote = index => {
    this.setState({
      modal: { isOpened: true, index: index, isEditing: false }
    });
  };

  closeNote = () => {
    this.setState({ modal: { isOpened: false } });
  };

  undo = () => {
    this.props.undo();
  };
  redo = () => {
    this.props.redo();
  };

  createNote = (title, body, done) => {
    this.props.createNote(
      title,
      body,
      done
    );
  };

  handleNotecreatorChange = event => {
    this.setState({
      noteForm: {
        ...this.state.noteForm,
        [event.target.id]: event.target.value
      }
    });
  };

  componentWillReceiveProps(props) {
    this.setState();
  }

  renderNoteList() {
    return (
      <span className="notes_centerer">
        {this.props.notes.present.notes.map((value, key) => (
          <Note
            state={value}
            index={key}
            onCompleat={this.onCompleat}
            onDelete={this.onDelete}
            onEdit={this.onEdit}
            onOpenNote={this.openNote}
          />
        ))}
      </span>
    );
  }

  render() {
    return (
      <div>
        <div className="content_container">
          {this.state.modal.isOpened ? (
            <NoteModal
              state={this.props.notes.present.notes[this.state.modal.index] || {state: undefined}}
              isEditing={this.state.modal.isEditing}
              onCompleat={this.onCompleat}
              onEdit={ this.state.modal.isEditing ? this.createNote : this.onEdit}
              onCloseNote={this.closeNote}
            />
          ) : null}
          {this.renderNoteList()}
        </div>

        <button onClick={() => this.setState({modal: {
          isOpened: true,
          isEditing: true,
        }})}>Создать заметку</button>
        <button onClick={this.undo}>Назад</button>
        <button onClick={this.redo}>Вперед</button>
      </div>
    );
  }
}

export default authRequired(
  connect(mapStateToProps, mapDispatchToProps)(Notes)
);
