import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TouchableHighlight,
  TextInput
} from "react-native";
import { connect } from "react-redux";
import CheckBox from "react-native-check-box";
import { createNote } from "../../../framework/modules/notes/actions";
import { bindActionCreators } from "redux";

function mapStateToProps(state, ownProps) {
  return state;
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ createNote }, dispatch);
}

type Props = {};
class NoteCreator extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      body: "",
      done: false
    };
  }

  createNote() {
    this.props
      .createNote(this.state.title, this.state.body, this.state.done)
      .then(() => {
        this.props.navigation.pop();
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.inputs}
          onChangeText={text => this.setState({ title: text })}
          value={this.state.title}
          placeholder="Заголовок"
        />
        <TextInput
          style={styles.inputs}
          onChangeText={text => this.setState({ body: text })}
          value={this.state.body}
          placeholder="Текст"
        />
        <CheckBox
          onClick={() => this.setState({ done: !this.state.done })}
          isChecked={this.state.done}
        />
        <Button title="Создать" onPress={() => this.createNote()} />
      </View>
    );
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NoteCreator);

const styles = StyleSheet.create({
  container: {
    flex: 3,
    margin: 5,
    flexDirection: "column",
    backgroundColor: "white"
  },
  done: {
    backgroundColor: "red",
    width: 15,
    height: 15,
    margin: 2
  },
  inputs: {
    height: 40,
    borderColor: "gray",
    borderWidth: 1
  }
});
