import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TouchableHighlight,
  TextInput,
  Alert
} from "react-native";
import { connect } from "react-redux";
import CheckBox from "react-native-check-box";
import {
  redo,
  undo,
  noteLocalUpdate,
  noteUpdate
} from "../../../framework/modules/notes/actions";
import { bindActionCreators } from "redux";

function mapStateToProps(state, ownProps) {
  return state;
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    { redo, undo, noteLocalUpdate, noteUpdate },
    dispatch
  );
}

type Props = {};
class NoteModal extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      title: this.props.notes.present.selectedNote.title,
      body: this.props.notes.present.selectedNote.body,
      done: this.props.notes.present.selectedNote.done
    };
  }

  saveNote() {
    this.props
      .noteUpdate(
        this.state.title,
        this.state.body,
        this.state.done,
        this.props.notes.present.selectedNote.id
      )
      .then(() => {
        this.props.navigation.replace("noteList");
      });
  }

  componentWillReceiveProps(props) {
    console.log(this.props);
    this.setState({ ...props.notes.present.selectedNote });
  }

  cancelChanges() {
    Alert.alert(
      "Изменения не сохранены",
      "Закрыть редактирование?",
      [
        { text: "Отмена" },
        {
          text: "Закрыть",
          onPress: () => this.props.navigation.replace("noteList")
        }
      ],
      { cancelable: true }
    );
  }

  renderTitleInput() {
    return (
      <TextInput
        style={styles.inputs}
        onChangeText={text =>
          this.props.noteLocalUpdate({
            title: text,
            body: this.state.body,
            done: this.state.done,
            id: this.props.notes.present.selectedNote.id
          })
        }
        value={this.state.title}
        placeholder="Заголовок"
      />
    );
  }
  renderBodyInput() {
    return (
      <TextInput
        style={styles.inputs}
        onChangeText={text =>
          this.props.noteLocalUpdate({
            title: this.state.title,
            body: text,
            done: this.state.done,
            id: this.props.notes.present.selectedNote.id
          })
        }
        value={this.state.body}
        placeholder="Текст"
      />
    );
  }
  render() {
    return (
      <View style={styles.container}>
        {this.renderTitleInput()}
        {this.renderBodyInput()}
        <CheckBox
          onClick={() =>
            this.props.noteLocalUpdate({
              title: this.state.title,
              body: this.state.body,
              done: !this.state.done,
              id: this.props.notes.present.selectedNote.id
            })
          }
          isChecked={this.state.done}
        />
        <Button title="Сохранить" onPress={() => this.saveNote()} />
        <Button title="Отмена" onPress={() => this.cancelChanges()} />
        <Button title="Вперед" onPress={() => this.props.redo()} />
        <Button title="Назад" onPress={() => this.props.undo()} />
      </View>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NoteModal);

const styles = StyleSheet.create({
  container: {
    flex: 3,
    margin: 5,
    backgroundColor: "white"
  },
  done: {
    backgroundColor: "red",
    width: 15,
    height: 15,
    margin: 2
  },
  inputs: {
    height: 40,
    borderColor: "gray",
    borderWidth: 1
  }
});
