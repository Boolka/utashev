import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import { connect } from "react-redux";
import NoteItem from "./noteItem";
import { AsyncStorage } from "react-native";
import { TOKEN } from "../../../framework/services/storeConsts";
import {
  getNotes,
  createNote,
  noteUpdate,
  noteLocalUpdate,
  deleteNote,
  undo,
  redo,
  setSelectedNote
} from "../../../framework/modules/notes/actions";
import {
  Platform,
  StyleSheet,
  Text,
  FlatList,
  View,
  Button
} from "react-native";
import { bindActionCreators } from "redux";

function mapStateToProps(state, ownProps) {
  return { state, AsyncStorage };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getNotes,
      createNote,
      noteUpdate,
      noteLocalUpdate,
      deleteNote,
      undo,
      redo,
      setSelectedNote
    },
    dispatch
  );
}

type Props = {};
class NoteList extends Component<Props> {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: "Список заметок",
    headerRight: (
      <Button
        onPress={() => navigation.navigate("createNote")}
        title="Добавить"
        color="#000000"
      />
    )
  });

  constructor(props) {
    super(props);
    this.checkAuth();
    this.props.getNotes();
  }

  async checkAuth() {
    const result = await this.props.AsyncStorage.getItem(TOKEN);
    if (!result) {
      this.props.navigation.replace("auth");
    }
  }

  undo = () => {
    this.props.undo();
  };
  redo = () => {
    this.props.redo();
  };

  createNote = (title, body, done) => {
    this.props.createNote(title, body, done);
  };

  onSelected = note => {
    console.log(this.props.navigation);
    this.props.navigation.replace("modalNote", {
      update: this.props.noteLocalUpdate,
      undo: this.props.undo,
      redo: this.props.redo
    });
    this.props.setSelectedNote(note);
  };

  render() {
    return (
      <View>
        {this.props.state.notes ? (
          <FlatList
            data={this.props.state.notes.present.notes}
            extraData={this.props.state}
            renderItem={({ item, index }) => {
              return (
                <NoteItem
                  state={item}
                  index={index}
                  onDelete={this.props.deleteNote}
                  onEdit={this.props.noteUpdate}
                  onSelected={this.onSelected}
                  undo={this.props.undo}
                  redo={this.props.redo}
                  update={this.props.noteLocalUpdate}
                  parentFlatList={this}
                />
              );
            }}
            keyExtractor={item => item.title}
            ItemSeparatorComponent={this.renderSeparator}
          />
        ) : (
          <Text> Заметок нет </Text>
        )}
      </View>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NoteList);
