import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TouchableHighlight
} from "react-native";
import CheckBox from "react-native-check-box";

type Props = {};
export default class NoteItem extends Component<Props> {
  render() {
    return (
      <TouchableHighlight
        onPress={() => {
          this.props.onSelected(this.props.state);
        }}
        underlayColor="white"
      >
        <View style={styles.container}>
          <Text>{this.props.state.title}</Text>
          <Text>{this.props.state.body}</Text>
          <CheckBox
            onClick={() =>
              this.props.onEdit(
                this.props.state.title,
                this.props.state.body,
                !this.props.state.done,
                this.props.state.id
              )
            }
            isChecked={this.props.state.done}
          />

          <TouchableHighlight
            onPress={() => this.props.onDelete(this.props.state.id)}
            underlayColor="white"
          >
            <View style={styles.deleteButton} />
          </TouchableHighlight>
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 3,
    margin: 5,
    flexDirection: "column",
    backgroundColor: "white"
  },
  done: {
    backgroundColor: "red",
    width: 15,
    height: 15,
    margin: 2
  },
  deleteButton: {
    alignSelf: "flex-end",
    aspectRatio: 1,
    backgroundColor: "red",
    width: 25,
    height: 25
  }
});
