import React, { Component } from "react";
import {
  Text,
  View,
  TextInput,
  Button,
  ScrollView
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  DrawerItems
} from "react-navigation";
import {
  syncDataWithStore,
  logout
} from "../../../framework/modules/auth/actions";

function mapStateToProps(state, ownProps) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ syncDataWithStore, logout }, dispatch);
}

class Sidebar extends Component {
  constructor(props) {
    super(props);
    if (props.auth.email === "") {
      this.props.syncDataWithStore();
    }
  }

  render() {
    return (
      <ScrollView>
        <DrawerItems {...this.props} />
        <Text>Email: {this.props.auth.email}</Text>
        <Button
          title="Выйти"
          onPress={() => {
            this.props.logout();
            this.props.navigation.replace("auth");
          }}
        />
      </ScrollView>
    );
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Sidebar);
