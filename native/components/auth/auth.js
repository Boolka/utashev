import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  AsyncStorage
} from "react-native";
import { connect } from "react-redux";
import { auth, logout } from "../../../framework/modules/auth/actions";

class Auth extends Component {
  constructor(props) {
    super(props);
    this.props.logout();
    this.state = {
      email: "",
      password: ""
    };

    this.onSignin = this.onSignin.bind(this);
  }

  async onSignin() {
    const result = await this.props.auth(this.state.email, this.state.password);
    if (result) {
      this.props.navigation.replace('noteList')
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.textInput}
          onChangeText={email => this.setState({ email: email })}
          placeholder="login"
        />
        <TextInput
          style={styles.textInput}
          onChangeText={password => this.setState({ password: password })}
          placeholder="Password"
          secureTextEntry={true}
        />
        <Button
          style={styles.buttonContainer}
          onPress={this.onSignin}
          title="Авторизоваться"
        />
        <Button
          style={styles.buttonContainer}
          onPress={() => this.props.navigation.navigate('registration')
        }
          title="Регистрация"
        />
      </View>
    );
  }
}
export default connect(
  state => ({ state: state.auth }),
  { auth, logout }
)(Auth);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignContent: "center"
  },
  textInput: {
    padding: 10,
    marginTop: 3
  }
});
