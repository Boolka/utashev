import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  AsyncStorage
} from "react-native";
import { connect } from "react-redux";
import { registration } from "../../../framework/modules/auth/actions";

class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
    this.onSignup = this.onSignup.bind(this);
  }

  async onSignup() {
    console.log(this.state);
    const result = await this.props.registration(
      this.state.email,
      this.state.password
    );
    if (result) {
      this.props.navigation.pop()
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.textInput}
          onChangeText={email => this.setState({ email: email })}
          placeholder="login"
        />
        <TextInput
          style={styles.textInput}
          onChangeText={password => this.setState({ password: password })}
          placeholder="Password"
          secureTextEntry={true}
        />
        <Button
          style={styles.buttonContainer}
          onPress={this.onSignup}
          title="Зарегистрироваться"
        />
        <Button
          style={styles.buttonContainer}
          onPress={() => this.props.navigation.pop()
          }
          title="Войти"
        />
      </View>
    );
  }
}
export default connect(
  state => ({ state: state }),
  { registration }
)(Registration);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignContent: "center"
  },
  textInput: {
    padding: 10,
    marginTop: 3
  }
});
