import React, { Component } from "react";
import { DrawerItems, SafeAreaView, StackNavigator, DrawerNavigator } from "react-navigation";
import { Button, ScrollView, Text} from 'react-native';
import Auth from "./components/auth/auth";
import noteList from "./components/notes/noteList";
import Registration from "./components/auth/registration";
import createNote from "./components/notes/noteCreator";
import noteModal from "./components/notes/noteModal";
import { connect } from "react-redux";
import Sidebar from "./components/sidebar/sidebar";
import { bindActionCreators } from 'redux';

function mapStateToProps(state, ownProps) {
  return state;
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({  }, dispatch);
}
const Drawer = DrawerNavigator({
  Главная: noteList
}, {
  contentComponent: Sidebar
})

const Routes = StackNavigator(
  {
    auth: { screen: Auth },
    registration: { screen: Registration },
    noteList: {
      screen: Drawer
    },
    createNote: { screen: createNote },
    modalNote: { screen: noteModal }
  },
  {
    initialRouteName: "noteList"
  }
);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Routes);