import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  AsyncStorage
} from 'react-native';
import { Provider } from "react-redux";
import Routes from "./native/routes";
import store from "./framework/store";
import Storage from "./framework/services/tokenService";
import Network from "./framework/services/networkService"

type Props = {};
export default class App extends Component<Props> {

  constructor(props) {
    super(props)
    const tokenService = new Storage(AsyncStorage);
    const networkService = new Network(tokenService);
    this.services = { tokenService, networkService }
  }

  render() {
    return (
      <Provider store={store(this.services)}>
        <Routes/>
      </Provider>
    );
  }
}