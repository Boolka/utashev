import * as TYPE from "./actionTypes";

var initialState = {
  userId: "",
  id: "",
  сrated: "",
  ttl: "",
  email: ""
};

export default (state = initialState, action) => {
  switch (action.type) {
    case TYPE.AUTH:
      return {
        email: action.payload
      };
    case TYPE.AUTH_FULFILLED:
      return {
        ...state,
        ...action.payload
      };
    case TYPE.AUTH_REJECT:
      return {
        ...state
      };
    case TYPE.REGISTRATION:
      return {
        ...state,
        ...action.payload
      };
    case TYPE.REGISTRATION_FULFILLED:
      return {
        ...state,
        ...action.payload
      };
    case TYPE.LOGOUT:
      return {};
    default:
      return state;
  }
};
