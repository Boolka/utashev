import * as TYPE from "./actionTypes";

export const auth = (email, password) => async (
  dispatch,
  getState,
  services
) => {
  const serverResponse = await services.networkService.process(
    "users/login",
    "POST",
    { email, password }
  );
  if (serverResponse.ok) {
    const response = await serverResponse.json();
    dispatch({
      type: TYPE.AUTH_FULFILLED,
      payload: response
    });
    services.tokenService.setToken(JSON.stringify(response));
    services.tokenService.saveEmail(email);
    return true;
  } else {
    dispatch({
      type: TYPE.AUTH_REJECT
    });
    return false;
  }
};

export const registration = (email, password) => async (
  dispatch,
  getState,
  services
) => {
  const serverResponse = await services.networkService.process(
    "users",
    "POST",
    { email, password }
  );
  if (serverResponse.ok) {
    const response = await serverResponse.json();
    dispatch({
      type: TYPE.REGISTRATION_FULFILLED,
      payload: response
    });
    return true;
  } else {
    dispatch({
      type: TYPE.REGISTRATION_REJECT
    });
    return false;
  }
};

export const logout = () => async (dispatch, getState, services) => {
  dispatch({
    type: TYPE.LOGOUT
  });
  services.tokenService.dropToken();
};

export const syncDataWithStore = () => async (dispatch, getState, services) => {
  const user = await services.tokenService.getEmail();
  dispatch({
    type: TYPE.AUTH,
    payload: user
  });
};
