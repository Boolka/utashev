import * as TYPE from "./actionTypes";
import { ActionCreators } from "redux-undo";

export const getNotes = () => async (dispatch, getState, services) => {
  const response = await services.networkService.process("tasks", "GET", null);
  if (response.ok) {
    const json = await response.json();
    dispatch({
      type: TYPE.NOTES_FULFILLED,
      payload: json
    });
    return json;
  } else {
    return dispatch({
      type: TYPE.NOTES_REJECT,
      payload: response
    });
  }
};

export const createNote = (title, body, done) => async (
  dispatch,
  getState,
  services
) => {
  const response = await services.networkService.process("tasks", "POST", {
    title: title,
    body: body,
    done: done
  });
  if (response.ok) {
    const json = await response.json();
    dispatch({
      type: TYPE.NOTE_CREATE_FULFILLED,
      payload: json
    });
  } else {
    dispatch({
      type: TYPE.NOTE_CREATE_REJECT,
      payload: []
    });
  }
};

export const deleteNote = id => async (dispatch, getState, services) => {
  let response = await services.networkService.process("tasks/" + id, "DELETE");
  if (response.ok) {
    var state = getState().notes.present.notes;
    var position;
    const newState = state.map((value, key) => {
      if (value.id == id) {
        position = key;
      }
      return value;
    });

    newState.splice(position, 1);
    dispatch({
      type: TYPE.NOTE_DELETE_FULFILLED,
      payload: newState
    });
  } else {
    dispatch({
      type: TYPE.NOTE_DELETE_REJECT,
      payload: state
    });
  }
};

export const noteLocalUpdate = note => (dispatch, getState, services) => {
  console.log(note)
  dispatch({
    type: TYPE.NOTE_LOCAL_UPDATE_FULFILLED,
    payload: note
  });
};

export const noteUpdate = (title, body, done, id) => async (
  dispatch,
  getState,
  services
) => {
  let response = await services.networkService.process("tasks/" + id, "PUT", {
    id: id,
    done: done,
    body: body,
    title: title
  });
  if (response.ok) {
    var state = getState();
    const newState = state.notes.present.notes.map((value, key) => {
      if (value.id == id) {
        return { id, title, body, done };
      } else {
        return value;
      }
    });
    dispatch({
      type: TYPE.NOTE_UPDATE_FULFILLED,
      payload: newState
    });
  } else {
    dispatch({
      type: TYPE.NOTE_UPDATE_REJECT,
      payload: state
    });
  }
};

export const setSelectedNote = note => dispatch => {
  dispatch({
    type: TYPE.NOTE_SELECT,
    payload: note
  });
};

export const undo = () => (dispatch, getState) => {
  dispatch(ActionCreators.undo());
};

export const redo = () => (dispatch, getState) => {
  dispatch(ActionCreators.redo());
};
