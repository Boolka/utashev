import * as TYPE from "./actionTypes";

const initialState = {
  notes: [],
  selectedNote: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case TYPE.NOTES_FULFILLED:
      return {
        notes: [...action.payload]
      };
    case TYPE.NOTE_COMPLEAT:
      return {
        notes: [...state.notes, ...action.payload]
      };
    case TYPE.NOTE_CREATE:
      return {
        notes: [...state.notes, ...action.payload]
      };
    case TYPE.NOTE_DELETE:
      return {
        notes: [...state.notes, ...action.payload]
      };
    case TYPE.NOTE_EDIT:
      return {
        ...state,
        selectedNote: action.payload
      };
    case TYPE.NOTE_SELECT:
      return {
        notes: [...state.notes],
        selectedNote: { ...action.payload }
      };
    case TYPE.SELECTED_NOTE_EDIT:
      return {
        notes: [...state.notes],
        selectedNote: { ...action.payload }
      };
    case TYPE.NOTE_CREATE_FULFILLED:
      return {
        notes: [...state.notes, action.payload]
      };
    case TYPE.NOTE_CREATE_REJECT:
      return {
        notes: [...state.notes]
      };
    case TYPE.NOTE_LOCAL_UPDATE_FULFILLED:
      return {
        notes: [...state.notes],
        selectedNote: action.payload
      };
    case TYPE.NOTE_UPDATE_FULFILLED:
      return {
        notes: [...action.payload],
        selectedNote: state.selectedNote
      };
    case TYPE.NOTE_UPDATE_REJECT:
      return {
        notes: [...state.notes],
        selectedNote: state.selectedNote
      };
    case TYPE.NOTE_DELETE_FULFILLED:
      return {
        notes: [...action.payload]
      };
    default:
      return state;
  }
};
