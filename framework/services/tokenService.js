import { connect } from "react-redux";
import { TOKEN, EMAIL } from "./storeConsts";

import "babel-polyfill";

export default class Storage {
  constructor(storage) {
    this.storage = storage;
  }

  async getTokenFullData() {
    const tokenResp = await this.storage.getItem(TOKEN);
    return tokenResp && tokenResp != "undefined" ? JSON.parse(tokenResp) : null;
  }

  async setToken(token) {
    this.storage.setItem(TOKEN, token);
  }

  async getToken() {
    const token = await this.storage.getItem(TOKEN);
    if (token && token != "undefined") {
      const parsed = JSON.parse(token);
      return parsed.id;
    }
    return null;
  }

  async checkToken() {
    const tokenInfo = await getTokenFullData();
    if (tokenInfo == null) {
      return false;
    }
    return true;
  }

  async dropToken() {
    this.storage.removeItem(TOKEN);
  }

  async saveEmail(email) {
    this.storage.setItem(EMAIL, email);
  }

  async getEmail() {
    const email = await this.storage.getItem(EMAIL);
    return email && email != "undefined" ? email : null;
  }
}
