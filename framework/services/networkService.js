const BASE_URL = "http://localhost:3000/api/";

export default class Network {
  constructor(storage) {
    this.storage = storage;
  }

  async process(url, method, body) {
    const token = await this.storage.getToken();
    let header = { "Content-Type": "application/json" };
    if (token) header = { ...header, access_token: token };

    if (method == "POST" || method == "PUT") {
      return await fetch(BASE_URL + url, {
        headers: header,
        method: method,
        body: body ? JSON.stringify(body) : {}
      });
    } else {
      return await fetch(BASE_URL + url, {
        headers: header,
        method: method
      });
    }
  }
}
