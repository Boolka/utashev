import { createStore, applyMiddleware } from "redux";
import { apiMiddleware } from "redux-api-middleware";

import thunk from "redux-thunk";
import { combineReducers } from "redux";
import auth from "./modules/auth/reducer";
import notes from "./modules/notes/reducer";
import undoable from "redux-undo";
import { composeWithDevTools } from "remote-redux-devtools";

const store = services =>
  createStore(
    combineReducers({
      auth,
      notes: undoable(notes)
    }),
    composeWithDevTools(
      applyMiddleware(apiMiddleware, thunk.withExtraArgument(services))
    )
  );
export default store;
